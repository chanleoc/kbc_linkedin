Extract Campaign Ad's statistics from LinkedIn Ads to Keboola Connection Platform (KBC).

### Retention
|timeGranularity|Demographic|Non-Demographic|
|-|-|-|
|DAILY|Last 6 months|Last 10 Years|
|MONTHLY|Last 2 years|Last 10 Years|
|YEARLY|Last 2 years|Last 10 Years|
|ALL|Last 2 years|Last 10 Years|

### Pre-Configured Templates: 

1. Ads Analytics
    - Users can extract campaign analytics with custom configured date range
    - #### This template will be `DEPRECATED` on July 31, 2020
2. Ads Analytics - Last 7 days
    - Extracting active campaigns ad analytics for the last 7 days
    - #### This template will be `DEPRECATED` on July 31, 2020
3. Ads Analytics V2
    - Extracting all campaign statistics with custom configured date range
4. Ads Analytics V2 - Last 7 days
    - Extracting all campaign statistics for the last 7 days

### Important Parameters:

1. Pivot
    - Pivot of the results which each report data point is grouped by
2. Time Granularity
    - Time granularity of the results
3. Start Year / Start Month / Start Day
    - The inclusive start time range of the analytics
    - Any inputs in these fields within the "Today"'s template will be ignored
4. End Year / End Month / End Day
    - The inclusive end time range of the analytics
    - Any inputs in these fields within the "Today"'s template will be ignored
        
**Note: Templates can be switched to JSON formatted template for customization.**

