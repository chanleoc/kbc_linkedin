# README #

This repository is a collection of configurations needed to register Keboola Generic Extractor as a branded LinkedIn KBC Extractor.
Extractor's task is to help user to extract the data from LinkedIn to Keboola Connection Platform (KBC). 

## API documentation ##
[LinkedIn Ads API documentation](https://developer.linkedin.com/docs/guide/v2/ads/ads-reporting)  

## Retention ##
|timeGranularity|Demographic|Non-Demographic|
|-|-|-|
|DAILY|Last 6 months|Last 10 Years|
|MONTHLY|Last 2 years|Last 10 Years|
|YEARLY|Last 2 years|Last 10 Years|
|ALL|Last 2 years|Last 10 Years|


## Configuration ##

There are two pre-configured templates for user to select. One allows user to extract data within a configured date range while the other one has configuration to extract current day's data (mainly for orchestration purposes).

Pre-Configured Templates: 

1. Ads Analytics V2
    - Extracting all campaign statistics with custom configured date range
2. Ads Analytics V2 - Last 7 days
    - Extracting all campaign statistics for the last 7 days

Required input information:

1. Pivot
    - Pivot of the results which each report data point is grouped by
2. Time Granularity
    - Time granularity of the results
3. Start Year / Start Month / Start Day
    - The inclusive start time range of the analytics
    - Any inputs in these fields within the "Today"'s template will be ignored
4. End Year / End Month / End Day
    - The inclusive end time range of the analytics
    - Any inputs in these fields within the "Today"'s template will be ignored
        
**Note: Templates can be switched to JSON formatted template for customization. Current templates are configured to extract "Active" campaign only.**


## Contact Info ##
Leo Chan  
Vancouver, Canada (PST time)   
Email: leo@keboola.com  
Private: cleojanten@hotmail.com   