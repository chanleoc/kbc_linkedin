import pandas as pd
import json
import sys

data_in  = pd.read_csv('linkedin_field_inputs.csv')
# print(data_in)

mapping_out = {}
fields_out = {}
base_fields = 'dateRange,pivotValue,pivot'

for index, row in data_in.iterrows():
    if not pd.isna(row['output_filename']):
        print('Mapping - {} to {}'.format(row['Field Name'], row['output_filename']))

        # injecting mapping:
        if row['output_filename'] not in mapping_out:
            mapping_out[row['output_filename']] = {}
        mapping_out[row['output_filename']][row['Field Name']] = {
            'type': 'column',
            'mapping': {
                'destination': '{}'.format(row['Field Name'])
            }
        }

        # injecting fields in job endpoints:
        if row['output_filename'] not in fields_out:
            fields_out[row['output_filename']] = base_fields
        fields = fields_out[row['output_filename']]
        fields_out[row['output_filename']] = fields + ',{}'.format(row['Field Name'])
        #print(mapping_out)
        #print(fields_out)
        #sys.exit(0)

jobs_out = []
for output_file in fields_out:
    job_skeleton_daily = {
        'endpoint': 'adAnalyticsV2?campaigns[0]=urn:li:sponsoredCampaign:{campaign_id}',
        'placeholders': {
            'campaign_id': 'id'
        },
        'dataField': 'elements',
        'dataType': output_file,
        'params': {
            'q': 'analytics',
            'timeGranularity': {
                'attr': 'timeGranularity'
            },
            'pivot': {
                'attr': 'pivot'
            },
            'dateRange.start.year': {
                'function': 'date',
                'args': [
                    'Y',
                    {
                        'function': 'strtotime',
                        'args': [
                            '-7 days'
                        ]
                    }
                ]
            },
            'dateRange.start.day': {
                'function': 'date',
                'args': [
                    'd',
                    {
                        'function': 'strtotime',
                        'args': [
                            '-7 days'
                        ]
                    }
                ]
            },
            'dateRange.start.month': {
                'function': 'date',
                'args': [
                    'm',
                    {
                        'function': 'strtotime',
                        'args': [
                            '-7 days'
                        ]
                    }
                ]
            },
            'fields': fields_out[output_file]
        }
    }
    job_skeleton = {
        'endpoint': 'adAnalyticsV2?campaigns[0]=urn:li:sponsoredCampaign:{campaign_id}',
        'placeholders': {
            'campaign_id': 'id'
        },
        'dataField': 'elements',
        'dataType': output_file,
        'params': {
            'q': 'analytics',
            'timeGranularity': {
                'attr': 'timeGranularity'
            },
            'pivot': {
                'attr': 'pivot'
            },
            'dateRange.start.year': {
                'attr': 'start_year'
            },
            'dateRange.end.day': {
                'attr': 'end_day'
            },
            'dateRange.end.month': {
                'attr': 'end_month'
            },
            'dateRange.end.year': {
                'attr': 'end_year'
            },
            'dateRange.start.day': {
                'attr': 'start_day'
            },
            'dateRange.start.month': {
                'attr': 'start_month'
            },
            'fields': fields_out[output_file]
        }
    }
    jobs_out.append(job_skeleton)

for mapping in mapping_out:
    # adding missing mapping (pks)
    missing_mapping = {
        'pivot': 'pivot',
        'pivotValue': 'pivotValue',
        'dateRange.start.year': 'start_year',
        'dateRange.start.month': 'start_month',
        'dateRange.start.day': 'start_day',
        'dateRange.end.year': 'end_year',
        'dateRange.end.month': 'end_month',
        'dateRange.end.day': 'end_day'
    }
    for value in missing_mapping:
        temp_json = {
            'type': 'column',
            'mapping': {
                'destination': missing_mapping[value],
                'primaryKey': True
            }
        }
        mapping_out[mapping][value] = temp_json

with open('mapping.json','w') as x:
    json.dump(mapping_out, x)
with open('jobs.json','w') as y:
    json.dump(jobs_out, y)